import airflow
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator

import pandas as pd


def upload_data_to_gcs():
    df = pd.DataFrame([
        [1,2,3,4],
        [5,6,7,8],
        [9,0,1,2]
    ])

    df.to_gbq(
        destination_table='test_dataset.test_table',
        project_id='buoyant-embassy-260717',
        if_exists='replace'
    )


default_args = {
    'owner': 'airflow',
    'start_date': airflow.utils.dates.days_ago(2),
    'retries': 2
}



with DAG('my_gcp_dag',
    default_args=default_args,
    schedule_interval='*/10 * * * *') as dag:
        opr_etl = PythonOperator(
            task_id='first_task',
            python_callable=upload_data_to_gcs
        )


opr_etl